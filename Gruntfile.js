module.exports = function(grunt) {
  "use strict";
  // Project configuration.
  var jsFiles = ['Gruntfile.js', 'src/js/**/*.js', '!src/js/external/*.js'];
  var resourceFiles = grunt.file.readJSON('src/resource/resource.json');
  var jsFilesList = resourceFiles.js.dev;
  var jsExtFilesList = resourceFiles.js.ext;

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        mangle: false
      },
      buildExt: resourceFiles.js.uglifyExtBuildOptions,
      build: resourceFiles.js.uglifyBuildOptions
    },
    clean: {
      js: {
        src: ["dist/js"]
      },
      transitionFiles: {
        src: ["dist/js/external/spot.ext.js", "dist/js/spot.js"]
      }
    },
    concat: {
      options: {
        separator: ';'
      },
      jsExt: {
        src: jsExtFilesList,
        dest: resourceFiles.js.uglifyExtBuildOptions.src
      },
      jsDev: {
        src: jsFilesList,
        dest: resourceFiles.js.uglifyBuildOptions.src
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.registerTask('build', ['clean:js', 'concat:jsExt', 'concat:jsDev', 'uglify:buildExt', 'uglify:build', 'clean:transitionFiles']);
  grunt.registerTask('buildDev', ['clean:js','concat:jsExt', 'concat:jsDev', 'uglify:buildExt', 'uglify:build']);

};
