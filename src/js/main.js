/*
* This is the main file which is executed at the start of application.
* Angular application is initialised within this file.
* */


/* ------------ Loading data into local storage ----------*/
if(window.localStorage.getItem("designations") === null) {
  $.ajax({
    url: "data/designations.json",
    type: "GET",
    async: false
  }).done(function (resp) {

    //Check added for apache servers which send response as string
    if(typeof resp === "string"){
      resp = JSON.parse(resp);
    }
    window.localStorage.setItem("designations", JSON.stringify(resp.designations));
  });
}

if(window.localStorage.getItem("employees") === null) {
  $.ajax({
    url: "data/employees.json",
    type: "GET",
    async: false
  }).done(function (resp) {
    //Check added for apache servers which send response as string
    if(typeof resp === "string"){
      resp = JSON.parse(resp);
    }
    window.localStorage.setItem("employees", JSON.stringify(resp.employees));
  });
}

if(window.localStorage.getItem("projects") === null) {
  $.ajax({
    url: "data/projects.json",
    type: "GET",
    async: false
  }).done(function (resp) {
    //Check added for apache servers which send response as string
    if(typeof resp === "string"){
      resp = JSON.parse(resp);
    }
    window.localStorage.setItem("projects", JSON.stringify(resp.projects));
  });
}
if(window.localStorage.getItem("tables") === null) {
  $.ajax({
    url: "data/tables.json",
    type: "GET",
    async: false
  }).done(function (resp) {
    //Check added for apache servers which send response as string
    if(typeof resp === "string"){
      resp = JSON.parse(resp);
    }
    window.localStorage.setItem("tables", JSON.stringify(resp.tables));
  });
}
if(window.localStorage.getItem("teams") === null) {
  $.ajax({
    url: "data/teams.json",
    type: "GET",
    async: false
  }).done(function (resp) {
    //Check added for apache servers which send response as string
    if(typeof resp === "string"){
      resp = JSON.parse(resp);
    }
    window.localStorage.setItem("teams", JSON.stringify(resp.teams));
  });
}

/*
* These ajax calls are kept to be synchronous so as to ensure all data is loaded before application starts
* This data can be loaded on the fly using commonService.js. This code can be found in commented methods.
* */

/*------------Loading data ends-----------------*/

var app = angular.module('App', ['Controllers', 'Services', 'ngRoute', 'ng-draggable']);
app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider){
  $locationProvider.html5Mode({
    enabled: true
  });
  $routeProvider.
      when(window.location.pathname, {
        templateUrl: window.location.pathname + 'src/views/home.html',
        controller: 'HomeController'
      });
}]);