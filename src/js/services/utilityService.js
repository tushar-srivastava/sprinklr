/*
* The Utility service contains common utility methods which are not specific to any view or controller and can be reused in the application
* */

Services.service('UtilityService', [function () {

  /*
  * Determines position of employee details tooltip.
  * By default it places tooltip on the right.
  * It then checks if the tooltip is within the viewport and changes the position accordingly
  * */
  this.getTooltipPosition = function(seatElement, mousePosition){
    var tableContainer = seatElement.closest(".js-tableContainer");
    var tableOffest = {
      left: tableContainer.offset().left,
      top: tableContainer.offset().top,
      right: tableContainer.offset().left + tableContainer.innerWidth(),
      bottom: tableContainer.offset().top + tableContainer.innerHeight()
    };

    var seatOffset = {
      left: seatElement.offset().left,
      top: seatElement.offset().top,
      right: seatElement.offset().left + seatElement.innerWidth(),
      bottom: seatElement.offset().top + seatElement.innerHeight()
    };
    var tooltipPosition = {
      x: mousePosition.x - tableOffest.left + 15,
      y: mousePosition.y - tableOffest.top - 30,
      class: "right"
    };
    if((mousePosition.x + 225) > $(window).innerWidth()){
      tooltipPosition.x = seatOffset.left - tableOffest.left - 220;
      tooltipPosition.class = "left";
    }
    if((seatOffset.top + 170) > $(window).innerHeight()){
      tooltipPosition.y = seatOffset.bottom - tableOffest.top - 170;
      tooltipPosition.class += " bottom";
    }
    return tooltipPosition;
  }
}]);