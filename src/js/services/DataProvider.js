var Services = angular.module('Services', []);

Services.service('DataProvider', ['$http',function ($http) {

  this.get = function(item) {
    var itemStr = window.localStorage.getItem(item);
    return JSON.parse(itemStr);
  };

  /*
  * The commented method will be used in actual application. It takes url and makes a get call
  * */
  /*this.get = function(url){
    return $.ajax({
      url: url,
      type: "GET"
    });
  };*/
}]);