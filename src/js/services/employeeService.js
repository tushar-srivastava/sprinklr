/*
* All the logic related to employee resides here. Provides the employee list, updates them whenever required.
* Provides details of a specific employee when required.
* */

Services.service('EmployeeService', ['DataProvider', 'CommonService',function (DataProvider) {
  this.employeesMap = {};
  this.employees = DataProvider.get("employees");
  this.getEmployees = function(){
    return this.employees;
  };

  //Updates the cached copy of employee list within the service instance
  this.updateEmployees = function() {
    this.employees = DataProvider.get("employees");
    this.convertIdToEmployeeMap();
  };

  //Generates an id->employee Object map. Makes it easier to fetch details of a single employee based on id
  this.convertIdToEmployeeMap = function(){
    var self = this;
    this.employees.map(function (v) {
      self.employeesMap[v.id] = v;
    });
  };
  this.convertIdToEmployeeMap();

  //Returns details of a single employee
  this.getUserDetails = function(id){
    return this.employeesMap[id];
  };
}]);