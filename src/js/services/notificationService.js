/*
* Manages notification of the application
* */

Services.service('NotificationService', [function () {
  this.showNotification = function(ele, options){
    ele.addClass(options.type).html(options.msg).fadeIn(400);
    window.setTimeout(function(){
      ele.fadeOut(400);
    }, options.duration || 3000);
  }
}]);