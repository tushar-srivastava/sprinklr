/*
* Kept as angular service as it has a single instance across the application.
*
* PURPOSE: Provides common methods to fetch data.
*
* COMMENTED Methods indicate that data can be fetched through ajax. These responses are cached within the instance so that repeated calls are
* not made for same data. However these methods can be forced to update the data by passing 'update' parameter as true
*
*
* */

Services.service('CommonService', ['DataProvider',function (DataProvider) {
  var self = this;
  this.teams = [];
  this.designations = [];
  this.tables = [];
  this.designationsMap = {};
  this.teamsMap = {};
  this.projectsMap = {};
  this.teams = DataProvider.get("teams");
  this.designations = DataProvider.get("designations");
  this.tables = DataProvider.get("tables");
  this.projects = DataProvider.get("projects");

  /* Converting designations array to id-->displayName map for fetching designation through id*/
  if(this.designations.length !== 0){
    for(var c=0; c< this.designations.length; c++){
      this.designationsMap[this.designations[c].id] = this.designations[c].displayName;
    }
  }

  if(this.teams.length !== 0){
    for(var c=0; c< this.teams.length; c++){
      this.teamsMap[this.teams[c].id] = this.teams[c].displayName;
    }
  }

  if(this.projects.length !== 0){
    for(var c=0; c< this.projects.length; c++){
      this.projectsMap[this.projects[c].id] = this.projects[c].name;
    }
  }


  /*
   * This method returns map of team id to display name
   * */
  this.getTeamsMap = function(){
    return this.teamsMap;
  };

  /*
   * This method returns map of project id to display name
   * */
  this.getProjectsMap = function(){
    return this.projectsMap;
  };

  /*
   * This method returns map of designation id to display name
   * */
  this.getDesignationsMap = function(){
    return this.designationsMap;
  };

  this.getTeams = function(){
    return this.teams;
  };

  this.getJobTitles = function(){
    return this.designations;
  };
  this.getTablesData = function(){
    return this.tables;
  };


  /*
  * This method is used to update the data in local storage taking key and data as parameters.
  * In actual application, this can be replaced by ajax request to update the data on server
  * */
  this.updateData = function(key, data){
    window.localStorage.setItem(key, JSON.stringify(data));
  };


  /*
  * The commented methods below will be used in actual application. They fetch respective data through ajax call.
  * The response is cached and checked whether its value is defined or not when the method is called again.
  * Deferred object is used to maintain consistency between the cases where data is fetched through ajax call and
  * where the data is fetched through cache.
  * */

   /*this.getTeams = function(update){
    var df = $.Deferred();
    if(this.teams.length === 0 || update === true){
      DataProvider.get("/data/teams.json").done(function(resp){
        self.teams = resp.teams;
        df.resolve(resp)
      });
    }else {
      window.setTimeout(function () {
        df.resolve({
          teams: self.teams
        });
      }, 100);
    }
    return df.promise();
  };*/


  /*this.getJobTitles = function(update){
    var df = $.Deferred();
    if(this.designations.length === 0 || update === true){
      DataProvider.get("/data/designations.json").done(function(resp){
        self.designations = resp.designations;
        df.resolve(resp)
      });
    }else {
      window.setTimeout(function () {
        df.resolve({
          designations: self.designations
        });
      }, 100);
    }
    return df.promise();
  };*/


  /*this.getTablesData = function(update){
    var df = $.Deferred();
    this.getJobTitles().done(function(){
      if(self.tables.length === 0 || update === true){
        DataProvider.get("/data/tables.json").done(function(resp){
          self.tables = resp.tables;
          df.resolve(resp);
        });
      }else {
        window.setTimeout(function () {
          df.resolve({
            tables: self.tables
          });
        }, 100);
      }
    });
    return df.promise();
  };*/
}]);


/*
* NOTE:
*       This service can further be broken down into individual services for team and projects for handling complex interactions.
*       This has been done for Employees. All the logic that is related to an employee has been separated inside EmployeeService.
* */