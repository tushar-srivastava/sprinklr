Controllers.controller('ThemeController', ['$scope', function($scope){
  $scope.themeColor = "blue";
  $scope.$watch('themeColor', function(){
    $scope.$emit('theme:changed', {
      color: $scope.themeColor
    });
  })
}]);