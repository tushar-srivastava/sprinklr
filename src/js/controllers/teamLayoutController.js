Controllers.controller('TeamLayoutController', ['$scope', 'CommonService', 'EmployeeService', function($scope, CommonService, EmployeeService){
  var self = this;
  $scope.tables = CommonService.getTablesData();
  $scope.designationMap = CommonService.getDesignationsMap();
  $scope.updateEmployeeData = function(id, projectId){
    var employees = EmployeeService.getEmployees();
    for(var c=0; c<employees.length; c++){
      var emp = employees[c];
      if(emp.id === id){
        emp.projectId = projectId;
        break;
      }
    }
    CommonService.updateData("employees", employees);
    EmployeeService.updateEmployees();
  };


  $scope.onDropSuccess = function(data, event){
    if(data !== undefined) {
      var tables = CommonService.getTablesData();
      var tableId = data.table.id;
      var employeeId = data.seat.employeeId;
      for (var c = 0; c < tables.length; c++) {
        if (tables[c].id === tableId) {
          for (var i = 0; i < tables[c].seats.length; i++) {
            if (tables[c].seats[i].employeeId === employeeId) {
              tables[c].seats[i].employeeId = null;
              break;
            }
          }
          break;
        }
      }
      var target = event.element;
      var seatContainer = $(target).closest(".css-seatContainer");
      var targetTable = seatContainer.closest(".css-tableContainer");
      var seatIndex = parseInt(seatContainer.data("index"));
      var targetTableId = parseInt(targetTable.attr("id"));
      for (var counter = 0; counter < tables.length; counter++) {
        if (tables[counter].id === targetTableId) {
          var projectId = tables[counter].projectId;
          tables[counter].seats[seatIndex].employeeId = employeeId;
          $scope.updateEmployeeData(employeeId, projectId);
          break;
        }
      }
      $scope.$broadcast('seat:updateSeat', {
        employeeId: employeeId,
        tableId: targetTableId,
        index: seatIndex
      });
      var emp = EmployeeService.getUserDetails(employeeId);
      if (tableId !== targetTableId) {
        $scope.$emit('showNotification', {
          type: 'success',
          msg: emp.name + ' has been moved from Table ' + tableId + " to Table " + targetTableId + ". A mail regarding this has been sent to " + emp.name
        });
      }
      $scope.tables = tables;
      CommonService.updateData("tables", tables);
    }
  }
}]);