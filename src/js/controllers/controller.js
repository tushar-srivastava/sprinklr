var Controllers = angular.module('Controllers', []);
Controllers.config(function($controllerProvider, $provide, $compileProvider){
  Controllers._controller = Controllers.controller;
  //Keeping the reference of controller function which can be utilized for registering controllers dynamically.
  Controllers.controller = function( name, constructor ) {
    $controllerProvider.register( name, constructor );
    return( this );
  };
});

/*
* Layout controller is the main controller. Any logic required on application level can be kept here
*/

Controllers.controller('LayoutController', ['$scope', 'NotificationService', function($scope, NotificationService){
  $scope.themeColor = "blue";
  $scope.$on('theme:changed', function(event, data){
    $scope.themeColor = data.color;
  });

  $scope.$on('showNotification', function(event, data){
    var ele = $("#notificationBlock");
    NotificationService.showNotification(ele, data);
  });
  $scope.getTemplateUrl = function(file){
    return window.location.pathname + 'src/views/partials/' + file;
  }
}]);