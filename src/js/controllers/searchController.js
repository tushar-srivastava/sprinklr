/*
* This controller is resposible for search view.
* on filter change, it emits a global event with the employee list as data, which signifies that filters have been changed.
* */

Controllers.controller('SearchController', ['$scope', '$rootScope', 'CommonService', 'EmployeeService', function($scope, $rootScope, CommonService, EmployeeService){
  $scope.searchString = "";
  $scope.teams = CommonService.getTeams();
  $scope.designations = CommonService.getJobTitles();
  $scope.employees = EmployeeService.getEmployees();
  $scope.filter = {
    teamsFilter: {},
    jobsFilter: {}
  };
  $scope.filterType = {
    team: false,
    jobTitle: false
  };

  //Function used to reset filter options
  $scope.resetFilterOptions = function(){
    var filter = {
      teamsFilter: {},
      jobsFilter: {}
    };
    for(var c=0; c<$scope.teams.length; c++){
      filter.teamsFilter[$scope.teams[c].id] = false;
    }
    for(var i=0; i<$scope.designations.length; i++){
      filter.jobsFilter[$scope.designations[i].id] = false;
    }
    $scope.filter = filter;
  };

  //Function which toggles between the filter types, i.e. Team and Job Title
  $scope.toggleFilterType = function(type){
    for(var key in $scope.filterType){
      if(key !== type){
        $scope.filterType[key] = false;
      }
    }
    $scope.resetFilterOptions();
  };
  $scope.filterObj = {
    teams: [],
    designations: []
  };

  //Makes sure the search string is empty before applying dropdown filters.
  $scope.onDropDownOpen = function () {
    $scope.searchString = "";
  };

  //Function that watches filter object and publishes change event on every change.
  $scope.$watch('filter', function(newVal, oldVal){
      $scope.filterObj.designations = [];
      $scope.filterObj.teams = [];
      if($scope.filterType.team === true) {

        var teamFilterArray = Object.keys($scope.filter.teamsFilter);
        teamFilterArray.map(function(v){
          if($scope.filter.teamsFilter[v] === true){
            $scope.filterObj.teams.push(v);
          }
        });
      }

      if($scope.filterType.jobTitle === true) {
        $scope.filterObj.teams = [];
        var designationsFilterArray = Object.keys($scope.filter.jobsFilter);
        designationsFilterArray.map(function(v){
          if($scope.filter.jobsFilter[v] === true){
            $scope.filterObj.designations.push(v);
          }
        });
      }
      $scope.filterEmployeeList();
  }, true);

  //Function that watches search string and resets dropdown filters.
  $scope.$watch('searchString', function(newVal, oldVal){
    $scope.resetFilterOptions();
    $scope.filterEmployeeList();
  });

  //Takes the current filters and gives and array of filtered employee Ids.
  $scope.filterEmployeeList = function() {
    var filteredList = [];
    for (var c = 0; c < $scope.employees.length; c++) {
      var emp = $scope.employees[c];
      if (($scope.searchString !== "" && emp.name.toLowerCase().indexOf($scope.searchString) !== -1) || ($scope.filterObj.teams.indexOf(emp.teamId) !== -1) || ($scope.filterObj.designations.indexOf(emp.designation) !== -1)) {
        filteredList.push(emp.id);
      }
    }
    $rootScope.$broadcast('rootScope:filterEmployees', filteredList);
  }
}]);