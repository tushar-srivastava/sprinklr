/*
* This controller contains all the logic for a particular seat, i.e., whether the seat is highlighted or not,
* to show employee details when mouse is rolled over and to update the seat when an employee is dragged over it
* */

Controllers.controller('SeatController', ['$scope', '$rootScope','EmployeeService', 'CommonService', 'UtilityService', function($scope, $rootScope, EmployeeService, CommonService, UtilityService){
  $scope.highlighted = false;
  $scope.currentFilteredList = [];
  $scope.init = function(empId, tableId, index){
    $scope.employeeId = empId;
    $scope.tableId = tableId;
    $scope.seatIndex = index;
  };

  //Called whenever any seat is changed. Main puropse of this function is to update the view of the changed seat according to the applied filters
  $scope.$on('seat:updateSeat', function(event, data){
    if(data.tableId === $scope.tableId){
      if($scope.seatIndex === data.index){
        $scope.employeeId = data.employeeId;
        highlightSeat();
      }
    }
  });

  //Called whenever the filter is changed. Takes list of employees and each seats sets the flag whether it is highlighted or not
  $rootScope.$on('rootScope:filterEmployees', function(event, data){
    $scope.currentFilteredList = data;
    highlightSeat();
  });

  //Function to show employee details whenever the mouse is rolled over.
  $scope.showEmployeeDetails = function(event, empId){
    var seatElement = $(event.target);
    var offset = {
      left: seatElement.offset().left,
      top: seatElement.offset().top,
      right: seatElement.offset().left + seatElement.innerWidth(),
      bottom: seatElement.offset().top + seatElement.innerHeight()
    };
    var tableContainer = seatElement.closest(".js-tableContainer");
    tableContainer.on('mousemove', function(e){
      var mousePosition = {
        x: e.pageX,
        y: e.pageY
      };

      if(mousePosition.x < offset.left || mousePosition.x > offset.right || mousePosition.y < offset.top || mousePosition.y > offset.bottom){
        $scope.hideEmployeeDetails(event);
        tableContainer.off('mousemove');
      }else {
        if(tableContainer.find(".js-employeeToolTip").length === 0) {
          var teamsMap = CommonService.getTeamsMap();
          var projectMap = CommonService.getProjectsMap();
          var designationMap = CommonService.getDesignationsMap();
          var userDetails = EmployeeService.getUserDetails(empId);
          var projectName = projectMap[userDetails.projectId];
          var team = teamsMap[userDetails.teamId];
          var designation = designationMap[userDetails.designation];
          var tooltipPosition = UtilityService.getTooltipPosition(seatElement, mousePosition);
          tableContainer.append('<div class="js-employeeToolTip css-employeeToolTip ' + tooltipPosition.class + '"><img src="' + userDetails.imageUrl + '"><name>' + userDetails.name + '</name><designation>' + designation + '</designation><label>Team</label><value>' + team + '</value><label>Current Project</label><value>' + projectName + '</value><div class="css-tooltipArrow"></div></div>');
          tableContainer.find(".js-employeeToolTip").css({
            "top": tooltipPosition.y + "px",
            "left": (tooltipPosition.x + 10) + "px"
          });
        }
      }
    });
  };

  $scope.hideEmployeeDetails = function(event){
    var seatElement = $(event.target);
    var tableContainer = seatElement.closest(".js-tableContainer");
    tableContainer.find(".js-employeeToolTip").remove();
  };
  function highlightSeat(){
    var data = $scope.currentFilteredList;
    if(data.length !== 0) {
      if ($scope.employeeId !== null) {
        if (data.indexOf($scope.employeeId) !== -1) {
          $scope.highlighted = true;
        }else {
          $scope.highlighted = false;
        }
      } else {
        $scope.highlighted = false;
      }
    }else {
      $scope.highlighted = false;
    }
  }
}]);