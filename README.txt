For running the app through zipped file:
---------------------------------------

  Extract all the files to the root folder of any running server, node/apache etc.

  The application can be accessed directly through index.html.

  In case of Apache server, access to image files might be needed.

========================================================================================================

For running the application by directly cloning the repo:
--------------------------------------------------------


  Run following commands to install dependencies, build and deploy:

  Pre-requisites: Node and bower should be installed

  1. git clone https://tushar-srivastava@bitbucket.org/tushar-srivastava/sprinklr.git
  2. cd <pwd>/sprinklr
  3. npm install
  4. bower install

  (if http-server module of node is not installed)
  5. npm install -g http-server

  6. http-server -p 3000


Application can now be accessed at localhost:3000


Build is carried out by running 'grunt build' command. It concatenates the js files and uglifies them. More tasks like obfuscation, beautifying, etc can be
added later on.